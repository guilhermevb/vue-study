var app = new Vue({
    el: '#app',
    data: {
        product: {
            selectedVariant: 0,
            about: 'https://www.google.com.br',
            details: ["100% algodão", "Quentinha", "Unisex"],
            sizes: ["P /", "M /", "G /", "GG"],
            variants: [
                {
                    variantId: 12345,
                    variantColor: 'blue',
                    variantImage: './assets/meia.jpg',
                    variantQuantity: 10,
                    brand: 'Duck´s',
                    name: 'Meia patos',
                    description: 'Meias personalizadas, com design de pato.',
                    alt: 'Meias de pato',
                },
                {
                    variantId: 23456,
                    variantColor: 'black',
                    variantImage: './assets/meiaMario.jpg',
                    variantQuantity: 0,
                    brand: 'SNES',
                    name: 'Meia Super Mario Bros',
                    description: 'Meias personalizadas, com design do Mario',
                    alt: 'Meias do Mario',
                }
            ],
            cart: 0
        }
    },
    methods: {
        addToCart() {
            this.product.cart += 1;
        },
        removeCart() {
            if (this.product.cart > 0) {
                this.product.cart -= 1;
            }
        },
        updateProduct(index) {
            this.product.selectedVariant = index
            console.log(index)
        }
    },
    computed: {
        title() {
            return this.product.variants[this.product.selectedVariant].brand + ' ' + this.product.variants[this.product.selectedVariant].name
        },
        description() {
            return this.product.variants[this.product.selectedVariant].description
        },
        image() {
            return this.product.variants[this.product.selectedVariant].variantImage
        },
        alt() {
            return this.product.variants[this.product.selectedVariant].alt
        },
        onSale() {
            return this.product.variants[this.product.selectedVariant].variantQuantity
        }
    }
})